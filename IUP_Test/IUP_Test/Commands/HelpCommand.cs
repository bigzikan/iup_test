﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IUP_Test.Commands
{
    public class HelpCommand
    {
        public HelpCommand()
        {
        }

        public void Run()
        {
            Console.WriteLine(
                $"{Constants.partCount} [путь к текстовому файлу без кавычек] - определение количества глав\r\n" +
                $"{Constants.sendMail} [путь к текстовому файлу без кавычек] [email] - выслать результат на почту"
                ) ;
        }
    }
}
