﻿using DocumentFormat.OpenXml.Wordprocessing;
using IUP_Test.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IUP_Test.Commands
{
    public class GetPartsNumberCommand
    {
        public string Path { get; set; }

        public GetPartsNumberCommand() 
        {
            Path = string.Empty;
        }        

        public void Run()
        {
            var service = new WordProcessingService(Path);
            Console.WriteLine(
                service.GetNumberPartsToConsole()
                ) ; 
        }
    }
}
