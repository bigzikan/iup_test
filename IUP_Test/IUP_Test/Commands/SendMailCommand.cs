﻿using IUP_Test.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IUP_Test.Commands
{
    public class SendMailCommand
    {
        public string EMail { get; set; }
        public string Path { get; set; }

        public SendMailCommand() 
        {
            EMail = string.Empty;
            Path = string.Empty;
        }

        public void Run()
        {
            var service = new SendMailService(EMail, Path);
            Console.WriteLine(
                service.GetSendMailResult()
                );
        }
    }
}
