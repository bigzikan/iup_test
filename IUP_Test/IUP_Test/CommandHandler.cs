﻿using IUP_Test.Commands;
using IUP_Test.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IUP_Test
{
    public class CommandHandler
    {
        private GetPartsNumberCommand _partsNumberCommandService;
        private HelpCommand _helpService;
        private SendMailCommand _sendMailCommandService;

        public CommandHandler()
        {
            _partsNumberCommandService = new GetPartsNumberCommand();
            _helpService = new HelpCommand();
            _sendMailCommandService = new SendMailCommand();
        }

        public void GetResponse(string command)
        {
            List<string> args = command.Split(' ').ToList();

            switch (args[0])
            {
                case Constants.help:
                    {
                        _helpService.Run();
                        break;
                    }
                case Constants.partCount:
                    {
                        if (args.Count < Constants.countArgsPartCountCommand)
                        {
                            throw new Exception("Необходимо указать путь к файлу (без кавычек)!");
                        }

                        _partsNumberCommandService.Path = args[1];
                        _partsNumberCommandService.Run();
                        break;
                    }
                case Constants.sendMail:
                    {
                        if (args.Count < Constants.countArgsSendMailCommand)
                        {
                            throw new Exception("Необходимо указать путь к файлу (без кавычек) и email!");
                        }

                        _sendMailCommandService.Path = args[1];
                        _sendMailCommandService.EMail = args[2];
                        _sendMailCommandService.Run();
                        break;
                    }
                default:
                    {
                        break;
                    }
            }
        }
    }
}
