﻿// See https://aka.ms/new-console-template for more information

using IUP_Test;

string? command = String.Empty;
var commandHandler = new CommandHandler();

while (true)
{
    Console.WriteLine("Введите запрос. Для справки введите \"help\"");
    command = Console.ReadLine();
    Console.WriteLine();

    if (command != null && command != String.Empty)
    {
        try
        {
            commandHandler.GetResponse(command);
        }
        catch(Exception ex)
        {
            Console.WriteLine(ex.ToString());
        }
        Console.WriteLine();
    }
}