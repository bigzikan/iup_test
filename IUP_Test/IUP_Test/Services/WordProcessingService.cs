﻿using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Wordprocessing;
using System.Text.Json;
using IUP_Test.Models;
using System.Text.Encodings.Web;
using System.Text.Unicode;

namespace IUP_Test.Services
{
    public class WordProcessingService
    {
        private string _path;
        private List<string> _partsNames;

        public WordProcessingService(string path)
        {
            _path = path;
            _partsNames = new List<string>();
            GetNumberPartsFromFile();
        }

        public string GetNumberPartsToConsole()
        {
            string resString = String.Join(Environment.NewLine, _partsNames);

            return $"{resString}{Environment.NewLine}Всего: {_partsNames.Count}";
        }

        public string? GetNumberPartsJson()
        {
            string result = string.Empty;

            PartsNamesResponse response = new PartsNamesResponse
            {
                partsNames = _partsNames,
                totalParts = _partsNames.Count
            };

            var serializerOptions = new JsonSerializerOptions
            {
                Encoder = JavaScriptEncoder.Create(UnicodeRanges.BasicLatin, UnicodeRanges.Cyrillic),
                WriteIndented = true
            };

            result = JsonSerializer.Serialize<PartsNamesResponse>(response, serializerOptions);

            return result;
        }

        public void GetNumberPartsFromFile()
        {
            var result = new List<string>();

            using (WordprocessingDocument wordDoc = WordprocessingDocument.Open(_path, true))
            {
                if (wordDoc == null)
                    return;

                IEnumerable<Paragraph> paragraphs = wordDoc.MainDocumentPart.Document.Body.OfType<Paragraph>();
                var Parts = paragraphs.Select(x => x).Where(p => p.ParagraphProperties != null && p.ParagraphProperties.NumberingProperties != null).ToList();
                var TextNamesParts = paragraphs.Select(x => x).Where(p => p.InnerText.Contains("Глава")).ToList();                

                int i = 1;

                foreach (var part in Parts)
                {
                    _partsNames.Add($"Глава {i}. {part.InnerText}");
                    i++;
                }

                foreach (var part in TextNamesParts)
                {
                    _partsNames.Add($"{part.InnerText.Replace(".", ". ")}");
                    i++;
                }
            }
        }
    }
}
