﻿using MailKit.Net.Smtp;
using MailKit.Security;
using MimeKit;
using MimeKit.Text;
using System.Net.Mail;
using System.Net;

namespace IUP_Test.Services
{
    public class SendMailService
    {
        private string _path;
        private string _email;
        private WordProcessingService _wordProcessingService;

        public SendMailService(string email, string path)
        {
            _email = email;
            _path = path;
            _wordProcessingService = new WordProcessingService(path);
        }

        public string GetSendMailResult()
        {
            string result = string.Empty;

            var json = _wordProcessingService.GetNumberPartsJson();

            if (json == null)
            {
                result = $"Отсутствует список глав!";
                return result;
            }

            var message = new MimeMessage();
            Console.WriteLine("Введите email отправителя: \r\n");
            var emailSender = Console.ReadLine();
            Console.WriteLine("Введите пароль почты: \r\n");
            var passwordSender = Console.ReadLine();

            message.From.Add(MailboxAddress.Parse($"{emailSender}"));
            message.To.Add(MailboxAddress.Parse($"{_email}"));
            message.Subject = "Количество глав в документе";
            message.Body = new TextPart(TextFormat.Plain) { Text = json };

            using (var client = new MailKit.Net.Smtp.SmtpClient())
            {
                client.Timeout = (int)TimeSpan.FromSeconds(5).TotalMilliseconds;
                client.Connect("smtp.mail.ru", 25, false);
                client.Authenticate(emailSender, passwordSender);
                client.Send(message);
                client.Disconnect(true);
            }

            return result;
        }
    }
}
