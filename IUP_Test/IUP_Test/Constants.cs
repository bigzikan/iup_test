﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IUP_Test
{
    public static class Constants
    {
        /// <summary>
        /// Вызов справки
        /// </summary>
        public const string help = "help";

        /// <summary>
        /// Определение количества глав
        /// </summary>
        public const string partCount = "partcount";

        /// <summary>
        /// Выслать сообщение на почту
        /// </summary>
        public const string sendMail = "sendmail";

        /// <summary>
        /// Количество аргументов в команде partcount
        /// </summary>
        public const int countArgsPartCountCommand = 2;

        /// <summary>
        /// Количество аргументов в команде sendMail
        /// </summary>
        public const int countArgsSendMailCommand = 3;
    }
}
