﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IUP_Test.Models
{
    public class PartsNamesResponse
    {
        public List<string> partsNames { get; set; }
        public int totalParts { get; set; }
    }
}
